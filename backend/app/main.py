import logging

from fastapi import Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.config import Settings, get_settings
from app.routers import router

logging.basicConfig(level=logging.INFO)

web_app = FastAPI()
web_app.include_router(router)

web_app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)


@web_app.get("/")
async def home(settings: Settings = Depends(get_settings)):
    return {"settings": settings.app_name}


