from typing import Optional
from fastapi import APIRouter, Depends
from app.dependencies import weather_client
from app.api_client import OpenWeatherAPIClient
from aiocache import Cache
from aiocache.serializers import JsonSerializer
import logging


router = APIRouter(prefix="/api")

cache = Cache(Cache.MEMORY, serializer=JsonSerializer(), ttl=3600)


@router.get("/weather-today")
async def get_weather(
    q: str, api_client: OpenWeatherAPIClient = Depends(weather_client)
):
    cache_key = f"{q}_current"
    if not (result := await cache.get(cache_key)):
        result = await api_client.get_current_weather(q)
        logging.info(f"API call with query: {q}")
        await cache.set(cache_key, result)
    return result


@router.get("/weather-forecast")
async def get_weather(
    q: str, api_client: OpenWeatherAPIClient = Depends(weather_client)
):
    cache_key = f"{q}_forecast"
    if not (result := await cache.get(cache_key)):
        result = await api_client.get_weather_forecast(q)
        logging.info("API call")
        await cache.set(cache_key, result, ttl=24 * 3600)
    return result
