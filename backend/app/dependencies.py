from app.config import get_settings
from app.api_client import OpenWeatherAPIClient


def weather_client():
    client = OpenWeatherAPIClient(api_key=get_settings().api_key)
    return client
