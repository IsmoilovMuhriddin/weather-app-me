import httpx


class APIClient:
    def __init__(self, api_key: str, base_url=""):
        self.api_key = api_key
        self.base_url = base_url


class OpenWeatherAPIClient(APIClient):
    class ENDPOINTS:
        CURRENT = "/weather"
        HOURLY_FORECAST = "/forecast"

    def __init__(self, api_key: str):
        base_url = "https://api.openweathermap.org/data/2.5"
        super().__init__(api_key, base_url)
        self.client = self._init_client()

    def _init_client(self):
        return httpx.AsyncClient()

    async def get_current_weather(self, q: str, units: str = "metric"):
        current_url = self.base_url + self.ENDPOINTS.CURRENT
        response = await self.client.get(
            current_url, params={"appid": self.api_key, "q": q, "units": units}
        )
        return response.json()

    async def get_weather_forecast(self, q: str, units: str = "metric"):
        current_url = self.base_url + self.ENDPOINTS.HOURLY_FORECAST
        response = await self.client.get(
            current_url, params={"appid": self.api_key, "q": q, "units": units}
        )
        return response.json()
